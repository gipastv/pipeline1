const inputTags = ["first_name","family_name","email_address","message"];

function modifyResetButton() {
	resetBtn = document.getElementById("reset");
	resetBtn.setAttribute("type","button");
	resetBtn.onclick = function() {resetFormular()};
}

function resetFormular() {
	let confirmed = confirm("Confirmez-vous la réinitialisation du formulaire?");
	if (confirmed == true) {
		for (let i=0; i < inputTags.length; i++) {
			document.getElementById(inputTags[i]).value = "";
		}
	}
}

modifyResetButton();





